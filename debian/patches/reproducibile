From: Michael R. Crusoe <crusoe@debian.org>
Subject: Make build reproducible
Forwarded: https://github.com/DaehwanKimLab/hisat2/pull/250
--- hisat2.orig/Makefile
+++ hisat2/Makefile
@@ -26,7 +26,7 @@
 CC = $(GCC_PREFIX)/gcc$(GCC_SUFFIX)
 CPP = $(GCC_PREFIX)/g++$(GCC_SUFFIX)
 CXX = $(CPP)
-HEADERS = $(wildcard *.h)
+HEADERS = $(sort $(wildcard *.h))
 BOWTIE_MM = 1
 BOWTIE_SHARED_MEM = 0
 
@@ -149,7 +149,7 @@
 HISAT2_BUILD_CPPS_MAIN = $(BUILD_CPPS) hisat2_build_main.cpp
 HISAT2_REPEAT_CPPS_MAIN = $(REPEAT_CPPS) $(BUILD_CPPS) hisat2_repeat_main.cpp
 
-SEARCH_FRAGMENTS = $(wildcard search_*_phase*.c)
+SEARCH_FRAGMENTS = $(sort $(wildcard search_*_phase*.c))
 VERSION = $(shell cat VERSION)
 
 # Convert BITS=?? to a -m flag
@@ -234,11 +234,11 @@
 	$(HT2LIB_DIR)/pymodule/ht2example.py
 
 
-GENERAL_LIST = $(wildcard scripts/*.sh) \
-	$(wildcard scripts/*.pl) \
-	$(wildcard *.py) \
-	$(wildcard example/index/*.ht2) \
-	$(wildcard example/reads/*.fa) \
+GENERAL_LIST = $(sort $(wildcard scripts/*.sh)) \
+	$(sort $(wildcard scripts/*.pl)) \
+	$(sort $(wildcard *.py)) \
+	$(sort $(wildcard example/index/*.ht2)) \
+	$(sort $(wildcard example/reads/*.fa)) \
 	example/reference/22_20-21M.fa \
 	example/reference/22_20-21M.snp \
 	$(PTHREAD_PKG) \
@@ -261,10 +261,10 @@
 # the Windows FIND tool instead.
 FIND=$(shell which find)
 
-SRC_PKG_LIST = $(wildcard *.h) \
-	$(wildcard *.hh) \
-	$(wildcard *.c) \
-	$(wildcard *.cpp) \
+SRC_PKG_LIST = $(sort $(wildcard *.h)) \
+	$(sort $(wildcard *.hh)) \
+	$(sort $(wildcard *.c)) \
+	$(sort $(wildcard *.cpp)) \
 	$(HT2LIB_PKG_SRC) \
 	Makefile \
 	CMakeLists.txt \
@@ -286,15 +286,20 @@
 
 repeat-debug: hisat2-repeat-debug
 
+BUILD_HOST ?= $(shell hostname)
+BUILD_TIME ?= $(shell date)
+
 DEFS=-fno-strict-aliasing \
      -DHISAT2_VERSION="\"`cat VERSION`\"" \
-     -DBUILD_HOST="\"`hostname`\"" \
-     -DBUILD_TIME="\"`date`\"" \
+     -DBUILD_HOST="\"${BUILD_HOST}\"" \
+     -DBUILD_TIME="\"${BUILD_TIME}\"" \
      -DCOMPILER_VERSION="\"`$(CXX) -v 2>&1 | tail -1`\"" \
      $(FILE_FLAGS) \
      $(PREF_DEF) \
      $(MM_DEF) \
-     $(SHMEM_DEF)
+     $(SHMEM_DEF) \
+     $(CXXFLAGS) \
+     $(CPPFLAGS)
 
 #
 # hisat-bp targets
